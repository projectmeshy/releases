# Meshy App (beta) Releases

Since, Meshy is currently not released in Play store or Windows store. You can download release apk/msix file directly from here and install it.

## Android

- Download [apk-release.apk](https://gitlab.com/projectmeshy/releases/-/raw/master/app-release.apk) and Install in Android mobile.
- Works on Android > v6.0

## Windows

- Download [meshy.msix](https://gitlab.com/projectmeshy/releases/-/raw/master/meshy.msix) installer package
- After downloading, Right click the meshy.msix. Select `Properties` option
- In `Properties` dialog, Select `Digital Signatures` tab.
- Double click `Msix Testing` file in signature list. Click `View certificate` in dialog box.
- Click `Install Certificate` and select `Local Machine` and proceed.
- In Certificate import wizard, Select `Place all certificates in the following stores` and Select `Browse`.
- In Browse dialog, Scroll down and find `Trusted People` and Click `Next` and `Finish`.
- After Importing, you get `Import was successful` and Click `ok` and leave the window.
- Now, Click the `meshy.msix` file to Install !

> This Installation process for windows is to make sure you're installing from right source. Since, I signed with my testing certificate, you get warning while you directly install meshy.msix. By installing Certificate as Trusted People, you're allowing meshy as safe to install. There is nothing to worry as I promise 0 virus/malware in Meshy.
